package com.example.routes

import com.example.data.UserSession
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.html.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import com.example.templates.HomeTemplate
import com.example.templates.LoginTemplate

fun Route.routesKtor() {
    // Redirect to login as per default
    get("/") {
        call.respondRedirect("/login")
    }

    // Display login form
    get("/login") {
        call.respondHtmlTemplate(LoginTemplate()) {}
    }

    // Handle login form submission
    authenticate("auth-form") {
        post("/login") {
            val userName = call.principal<UserIdPrincipal>()?.name.toString()
            call.sessions.set(UserSession(name = userName, count = 1))
            call.respondRedirect("/home")
        }
    }

    // Handle home page
    authenticate("auth-session") {
        get("/home") {
            val userSession = call.principal<UserSession>()
            call.sessions.set(userSession?.copy(count = userSession.count + 1))
            call.respondHtmlTemplate(HomeTemplate(userSession?.name, userSession?.count ?: 0)) {}
        }
    }
}

